# Chaos-Post Beschwerdeformular

Das Chaos-Post Beschwerdeformular in LaTeX als aufüllbares PDF mit digitalem Signaturfeld.

Benötigt folgende Pakete:
* expl3
* xcoffins
* geometry
* fontspec
* color
* ragged2e
* hyperref
* eforms (nicht in TeXLive enthalten, aber auf CTAN erhältlich)
* tikz (für die Falzmarken)

Als Schrift wird IBM Plex Sans verwendet.

# ToDo
* Siegel als Option
* Kopierer-Look

Für das Formular in Context, siehe: https://codeberg.org/fiee/context-examples/src/branch/master/form